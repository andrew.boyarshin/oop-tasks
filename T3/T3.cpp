﻿#if defined(_DEBUG) && defined(DEBUG_CRT)
#define new new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "../T2/T2.h"

#include <list>
#include <format>
#include <iostream>
#include <string>
#include <ranges>
#include <random>
#include <array>
#include <functional>

#include "../ut.hpp"

#include "random.h"

template <class T, std::enable_if_t<std::is_base_of_v<arithmetic_task, T>>* = nullptr>
object* create_arithmetic(std::default_random_engine& random)
{
    static std::uniform_real_distribution<arithmetic_task::result_type> reals;
    return new T(reals(random), reals(random));
}

void workload()
{
    using namespace boost::ut;

    expect(eq(0, object::count()));

    {
        std::shared_ptr<list<std::shared_ptr<object>>> tasks(new list<std::shared_ptr<object>>());

        std::size_t initial_task_count;
        std::size_t extra_objects = 0;

        {
            std::random_device rd;
            std::default_random_engine gen(rd());

            initial_task_count = std::uniform_int_distribution<std::size_t>(10, 20)(gen);

            const std::size_t task_type_count = 23;

            std::uniform_int_distribution<std::size_t> task_types(0, task_type_count - 1);

            std::array<std::move_only_function<object* (std::default_random_engine&)>, task_type_count> ctors =
            {
                &create_arithmetic<arithmetic_addition_task>,
                &create_arithmetic<arithmetic_addition_task>,
                &create_arithmetic<arithmetic_addition_task>,
                &create_arithmetic<arithmetic_subtraction_task>,
                &create_arithmetic<arithmetic_subtraction_task>,
                &create_arithmetic<arithmetic_subtraction_task>,
                &create_arithmetic<arithmetic_multiplication_task>,
                &create_arithmetic<arithmetic_multiplication_task>,
                &create_arithmetic<arithmetic_multiplication_task>,
                &create_arithmetic<arithmetic_division_task>,
                &create_arithmetic<arithmetic_division_task>,
                &create_arithmetic<arithmetic_division_task>,

                [&tasks](std::default_random_engine&) { return new object_count_task(tasks); },
                [&tasks](std::default_random_engine&) { return new object_count_task(tasks); },
                [&tasks](std::default_random_engine&) { return new task_func_count_task(tasks); },
                [&tasks](std::default_random_engine&) { return new task_func_count_task(tasks); },
                [&tasks](std::default_random_engine&) { return new task_func_count_task(tasks); },
                [](std::default_random_engine&) { return new total_object_count_task(); },
                [](std::default_random_engine&) { return new total_object_count_task(); },
                [](std::default_random_engine&) { return new total_object_count_task(); },

                [&tasks, &ctors, &task_types, &extra_objects](std::default_random_engine& random)
                {
                    ++extra_objects;
                    return new queue_task(tasks, std::shared_ptr<object>(ctors[task_types(random)](random)));
                },
                [&tasks, &ctors, &task_types, &extra_objects](std::default_random_engine& random)
                {
                    ++extra_objects;
                    return new queue_task(tasks, std::shared_ptr<object>(ctors[task_types(random)](random)));
                },

                [&tasks](std::default_random_engine&) { return new clear_task(tasks); }
            };

            static_assert(task_type_count == ctors.size());

            std::uniform_int_distribution<std::size_t> adjective_indices(0, adjectives.size() - 1);
            std::uniform_int_distribution<std::size_t> noun_indices(0, nouns.size() - 1);

            for (std::size_t i = 0; i < initial_task_count; ++i)
            {
                auto* task = ctors[task_types(gen)](gen);

                if (auto* named_task = dynamic_cast<named*>(task); named_task)
                    named_task->name(std::format("{} {}", adjectives[adjective_indices(gen)], nouns[noun_indices(gen)]));

                tasks->emplace_back(task);
            }
        }

        expect(eq(initial_task_count + extra_objects, object::count()));
        std::cout << "Object count: " << object::count() << " (" << initial_task_count << " queued)" << std::endl;

        list<std::string> info;

        auto process_item = [&info](auto&& task)
        {
            task->execute();
            info.emplace_back(task->format());
        };

        expect(!tasks->empty());
        expect(info.empty());

        while (!tasks->empty())
        {
            auto& front = tasks->front();

            if (auto arithmetic = std::dynamic_pointer_cast<task<double>>(front); arithmetic)
                process_item(arithmetic);
            else if (auto action = std::dynamic_pointer_cast<task<>>(front); action)
                process_item(action);
            else if (auto counting = std::dynamic_pointer_cast<task<std::size_t>>(front); counting)
                process_item(counting);
            else
                throw std::runtime_error(std::format("Unexpected task type"));

            if (!tasks->empty())
                tasks->pop_front();
        }

        expect(tasks->empty());

        while (!info.empty())
        {
            std::cout << info.front() << std::endl;
            info.pop_front();
        }

        expect(tasks->empty());
        expect(info.empty());
    }

    std::cout << "Object count: " << object::count() << std::endl;
    expect(eq(0, object::count()));
}

int main()
{
#if defined(_DEBUG) && defined(DEBUG_CRT)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_EVERY_16_DF | _CRTDBG_CHECK_CRT_DF);

    _CrtMemState s1, s2, s3;
    _CrtMemCheckpoint(&s1);
#elif !defined(_DEBUG)
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
#endif

    workload();

#if defined(_DEBUG) && defined(DEBUG_CRT)
    _CrtMemCheckpoint(&s2);

    if (_CrtMemDifference(&s3, &s1, &s2))
        _CrtMemDumpStatistics(&s3);
#endif

    return 0;
}
