﻿# Практические задания по спецкурсу ООП

## Требования

* CMake 3.22+
* Компилятор с полной поддержкой C++20 и [std::move_only_function](https://en.cppreference.com/w/cpp/utility/functional/move_only_function) из C++23
  * На момент написания данного документа — это только MSVC 19.32 (Visual Studio 17.2.0 Preview 2)
  * Таблица поддержки на [cppreference](https://en.cppreference.com/w/cpp/compiler_support)

## Замечания

* Надеюсь, я правильно проинтерпретировал условия 2 и 3 заданий (их условия содержат неоднозначные формулировки)
* Код портабельный во всех конфигурациях кроме Debug-ASAN и Debug-CRT
* Требуемые оценки сложности в первом задании выполняются только для не-дебаговой конфигурации
  * Первое задание содержит много кода для проверок целостности структуры в дебаговой конфигурации
  * Не реализованы проверки «валиден ли до сих пор итератор», их реализация сильно усложнила бы и без того объёмное задание (пришлось бы сделать пул итераторов)
* Использованы 2 сторонние библиотеки: [boost::μt](https://github.com/boost-ext/ut) и [nanobench](https://github.com/martinus/nanobench), исключительно для тестов и бенчмарков
* Для генерации имён задач в задании 3 был использован набор прилагательных и существительных под лицензией ISC из [aceakash/project-name-generator](https://github.com/aceakash/project-name-generator/tree/efe8c0e07fe466472db6529ddf280302e3b33a0b)

## Производительность (задание 1)

Сравнение проводится с реализацией `std::list` из [Microsoft STL](https://github.com/microsoft/STL) в x64 конфигурации на Intel Core i5-8400 (Coffee Lake) и Windows 11 (сборка 22563).

### Debug

Моя реализация уступает по производительности реализации `std::list`.
Стоит отметить, что все операции в моей реализации проводят значительно больше проверок целостности, чем аналогичные в STL.

### Release

Моя реализация обходит по производительности `std::list` во всех бенчмарках с любыми размерами хранимых структур данных.
