﻿#if defined(_DEBUG) && defined(DEBUG_CRT)
#define new new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "T1.h"

#include <list>
#include <format>
#include <iostream>
#include <string>
#include <ranges>

#include "../nanobench.h"
#include "../ut.hpp"

template <class It>
concept std_list_iterator = std::bidirectional_iterator<It> && !std::is_scalar_v<It> && !std::is_array_v<It>;

template <std_list_iterator It>
constexpr std::ostream& operator<<(std::ostream& os, const It& obj)
{
    return os << *obj;
}

static_assert(std_list_iterator<list_iterator<int, false>>);
static_assert(std_list_iterator<list_iterator<int, true>>);
static_assert(std_list_iterator<list_iterator<const int, false>>);
static_assert(std_list_iterator<list_iterator<const int, true>>);

template <class T>
auto produce_test(ankerl::nanobench::Bench& b, const std::string& full_name, const std::string& name, const auto& op,
                  const auto& check, const auto& constructor)
{
    static_assert(std::bidirectional_iterator<typename T::iterator>);
    static_assert(std::bidirectional_iterator<typename T::const_iterator>);
    static_assert(std::bidirectional_iterator<typename T::reverse_iterator>);
    static_assert(std::bidirectional_iterator<typename T::const_reverse_iterator>);

    const std::string test_name = full_name + "-" + name;
    boost::ut::detail::test{"test", test_name} = [&b, &op, &name, &check, &constructor]
    {
        auto constructor_specific = constructor(T());

        constructor_specific(
            [&op, &check](auto& list)
            {
                auto check_specific = check(list);
                op(list, check_specific, std::true_type())(list, check_specific);
            }
        );

        auto check_bench = []
        {
        };
        auto op_specific_bench = op(T(), check_bench, std::false_type());
        b.run(
            name, [&constructor_specific, &op_specific_bench, &check_bench]
            {
                constructor_specific(
                    [&op_specific_bench, &check_bench](auto& list)
                    {
                        op_specific_bench(list, check_bench);
                    }
                );
            }
        );
    };
}

template <typename T>
void test_impl(ankerl::nanobench::Bench& b, const std::string& name, const auto& op)
{
    const auto full_name = std::format("{}<{}>", name, typeid(T).name());
    b.title(full_name);

    std::vector<std::vector<T>> checkpoints;

    auto constructor = []<class List>(const List&)
    {
        return [](const auto& work)
        {
            List list;
            work(list);
        };
    };

    produce_test<std::list<T>>(
        b, full_name, "std", op,
        [&checkpoints](const auto& list)
        {
            return [&checkpoints, &list] { checkpoints.emplace_back(list.cbegin(), list.cend()); };
        },
        constructor
    );

    std::size_t i = 0;

#if defined(_DEBUG) && defined(DEBUG_CRT)
    _CrtMemState s1, s2, s3;
    _CrtMemCheckpoint(&s1);
#endif

    produce_test<list<T>>(
        b, full_name, "mine", op,
        [&checkpoints, &i](const auto& list)
        {
            return [&checkpoints, &list, &i]
            {
                using namespace boost::ut;

                const auto& reference = checkpoints[i++];

                expect(eq(list.size(), reference.size()));
                expect(std::equal(list.cbegin(), list.cend(), reference.cbegin()));
            };
        },
        constructor
    );

#if defined(_DEBUG) && defined(DEBUG_CRT)
    _CrtMemCheckpoint(&s2);

    if (_CrtMemDifference(&s3, &s1, &s2))
        _CrtMemDumpStatistics(&s3);
#endif

    boost::ut::expect(boost::ut::eq(i, checkpoints.size()));
}

void test_iterate(ankerl::nanobench::Bench&, const std::string&, const auto&)
{
}

template <typename Arg1, typename... Args>
void test_iterate(ankerl::nanobench::Bench& b, const std::string& name, const auto& op)
{
    test_impl<Arg1>(b, name, op);
    test_iterate<Args...>(b, name, op);
}

struct test_item_1 final
{
    std::uint64_t a;
    std::uint64_t b;

    [[nodiscard]] explicit test_item_1(auto v)
    {
        const auto uint64 = static_cast<uint64_t>(v);
        a = uint64 / 10;
        b = uint64 % 10;
    }

    friend std::ostream& operator<<(std::ostream& os, const test_item_1& obj)
    {
        return os << obj.a << ' ' << obj.b;
    }

    friend bool operator==(const test_item_1& lhs, const test_item_1& rhs)
    {
        return lhs.a == rhs.a
            && lhs.b == rhs.b;
    }

    template <class T, std::enable_if_t<std::is_arithmetic_v<T>>* = nullptr>
    friend bool operator==(const test_item_1& lhs, T rhs)
    {
        const auto uint64 = static_cast<uint64_t>(rhs);
        return lhs.a == uint64 / 10 && lhs.b == uint64 % 10;
    }

    auto operator<=>(const test_item_1&) const = default;
};

#if _DEBUG
template <class CharT>
struct std::formatter<test_item_1, CharT> : std::formatter<std::string, CharT>
{
    template <typename FormatParseContext>
    static auto parse(FormatParseContext& pc)
    {
        return pc.end();
    }

    template <class FormatContext>
    auto format(const test_item_1& t, FormatContext& fc) const
    {
        return std::format_to(fc.out(), "{} {}", t.a, t.b);
    }
};
#endif

struct test_item_2 final
{
    std::array<std::uint64_t, 100> a;

    [[nodiscard]] explicit test_item_2(auto v) : a()
    {
        a.fill(static_cast<std::uint64_t>(v));
    }

    friend std::ostream& operator<<(std::ostream& os, const test_item_2& obj)
    {
        return os << obj.a.size();
    }

    friend bool operator==(const test_item_2& lhs, const test_item_2& rhs)
    {
        return lhs.a == rhs.a;
    }

    template <class T, std::enable_if_t<std::is_arithmetic_v<T>>* = nullptr>
    friend bool operator==(const test_item_2& lhs, T rhs)
    {
        const auto uint64 = static_cast<uint64_t>(rhs);
        return lhs.a.front() == uint64 && lhs.a.back() == uint64;
    }

    auto operator<=>(const test_item_2&) const = default;
};

#if _DEBUG
template <class CharT>
struct std::formatter<test_item_2, CharT>
{
    template <typename FormatParseContext>
    static auto parse(FormatParseContext& pc)
    {
        return pc.end();
    }

    template <class FormatContext>
    auto format(const test_item_2& t, FormatContext& fc) const
    {
        return std::format_to(fc.out(), "{}", t.a.size());
    }
};
#endif

void test(std::string name, auto op)
{
    ankerl::nanobench::Bench b;
#ifdef _DEBUG
    b.minEpochIterations(10);
    b.warmup(10);
#else
    b.minEpochIterations(10000);
    b.warmup(1000);
#endif
    test_iterate<int, double, test_item_1, test_item_2>(b, name, op);
}

template <bool Test>
void test_empty(auto& list, auto& check)
{
    using namespace boost::ut;

    if constexpr (Test)
    {
        check();
        expect(list.empty());
        expect(eq(list.size(), 0));
        expect(ge(list.max_size(), list.size()));
        expect(ge(list.max_size(), 1024));
        expect(le(list.max_size(), static_cast<std::uintptr_t>(INTPTR_MAX)));
        expect(le(list.max_size(), UINTPTR_MAX / sizeof std::remove_cvref_t<decltype(list)>::value_type));
        expect(eq(list.begin(), list.end()));
        expect(eq(list.cbegin(), list.cend()));
        expect(eq(list.rbegin(), list.rend()));
        expect(eq(list.crbegin(), list.crend()));
        check();
    }
    else
    {
        ankerl::nanobench::doNotOptimizeAway(list);
    }
}

template <bool Test>
void test_operations(auto& list, auto& check)
{
    using namespace boost::ut;
    using value_type = typename std::remove_cvref_t<decltype(list)>::value_type;

    if constexpr (Test)
    {
        check();
        expect(list.empty());
        expect(eq(list.size(), 0));
        expect(eq(list.begin(), list.end()));
        expect(eq(list.cbegin(), list.cend()));
    }

    list.emplace_back(12);

    if constexpr (Test)
    {
        check();
        expect(not list.empty());
        expect(eq(list.size(), 1));
        expect(neq(list.begin(), list.end()));
        expect(neq(list.cbegin(), list.cend()));
    }

    list.emplace_back(42);

    if constexpr (Test)
    {
        check();
        expect(eq(list.size(), 2));
    }

    list.emplace_back(1024);

    if constexpr (Test)
    {
        check();
        expect(not list.empty());
        expect(eq(list.size(), 3));
        expect(neq(list.begin(), list.end()));
        expect(neq(list.cbegin(), list.cend()));
    }

    std::size_t index = 0;
    for (auto&& item : list)
    {
        static_assert(std::is_same_v<std::remove_cvref_t<decltype(item)>, value_type>);

        if constexpr (Test)
        {
            if (index == 0)
            {
                expect(item == 12_i);
                ++index;
                continue;
            }

            if (index == 1)
            {
                expect(item == 42_i);
                ++index;
                continue;
            }

            if (index == 2)
            {
                expect(item == 1024_i);
                ++index;
                continue;
            }

            expect(false);
        }
        else
        {
            ankerl::nanobench::doNotOptimizeAway(item);
        }
    }

    if constexpr (Test)
    {
        check();
        expect(eq(index, 3));
        expect(eq(list.size(), 3));
    }

    std::remove_cvref_t<decltype(list)> copy_1{list.begin(), list.end()};

    list.push_back(value_type(2048));
    list.push_back(value_type(4096));
    list.push_back(value_type(8192));

    if constexpr (Test)
    {
        check();
        expect(eq(list.size(), 6));
    }

    list.push_front(std::move(value_type(2)));
    list.push_front(std::move(value_type(4)));
    list.push_front(std::move(value_type(8)));

    if constexpr (Test)
    {
        check();
        expect(eq(list.size(), 9));
    }

    list.pop_front();
    list.pop_front();

    if constexpr (Test)
    {
        check();
        expect(eq(list.size(), 7));
    }

    list.pop_back();
    list.pop_back();

    if constexpr (Test)
    {
        check();
        expect(eq(list.size(), 5));
    }

    auto copy_2 = list;

    if constexpr (Test)
    {
        check();
        expect(eq(list, copy_2));
        expect(eq(list.size(), 5));
        expect(eq(copy_2.size(), 5));
    }

    copy_2.pop_front();
    copy_2.pop_back();

    if constexpr (Test)
    {
        check();
        expect(neq(list, copy_2));
        expect(eq(list.size(), 5));
        expect(eq(copy_2.size(), 3));
    }

    std::swap(list, copy_2);

    if constexpr (Test)
    {
        check();
        expect(neq(list, copy_2));
        expect(eq(list.size(), 3));
        expect(eq(copy_2.size(), 5));
        expect(eq(list, copy_1));
        expect(ge(list, copy_1));
        expect(le(list, copy_1));
        expect(neq(copy_2, copy_1));
        expect(gt(copy_2.size(), copy_1.size()));
        expect(lt(copy_2, copy_1));
    }
}

template <bool Test>
void test_invariants(auto& list, auto& check)
{
    using namespace boost::ut;
    using value_type = typename std::remove_cvref_t<decltype(list)>::value_type;

    if constexpr (Test)
    {
        check();
    }

    list.emplace_back(16);

    if constexpr (Test)
    {
        check();
    }

    list.emplace_back(32);

    if constexpr (Test)
    {
        check();
    }

    list.emplace_back(64);

    if constexpr (Test)
    {
        check();
    }

    list.reverse();

    if constexpr (Test)
    {
        check();
    }

    auto& a1 = list;
    auto a2 = list;
    a1 = {value_type(1), value_type(2), value_type(3)};
    a2 = {value_type(4), value_type(5)};

    if constexpr (Test)
    {
        check();
    }

    auto it1 = std::next(a1.begin());
    auto it2 = std::next(a2.begin());

    auto& ref1 = a1.front();
    auto& ref2 = a2.front();

    if constexpr (Test)
    {
        check();
        expect(neq(a1, a2));
        expect(eq(a1, std::remove_cvref_t<decltype(a1)>{value_type(1), value_type(2), value_type(3)}));
        expect(eq(a2, std::remove_cvref_t<decltype(a2)>{value_type(4), value_type(5)}));
        expect(eq(*it1, 2));
        expect(eq(*it2, 5));
        expect(eq(ref1, 1));
        expect(eq(ref2, 4));
    }

    a1.swap(a2);

    // Note that after swap the iterators and references stay associated with their
    // original elements, e.g. it1 that pointed to an element in 'a1' with value 2
    // still points to the same element, though this element was moved into 'a2'.

    if constexpr (Test)
    {
        check();
        expect(neq(a1, a2));
        expect(eq(a1, std::remove_cvref_t<decltype(a1)>{value_type(4), value_type(5)}));
        expect(eq(a2, std::remove_cvref_t<decltype(a2)>{value_type(1), value_type(2), value_type(3)}));
        expect(eq(*it1, 2));
        expect(eq(*it2, 5));
        expect(eq(ref1, 1));
        expect(eq(ref2, 4));
    }
}

#define WRAP_FUNC(name) ([]<class List, class Check, class Test>(const List&, const Check&, const Test&) { return &name<Test::value, List, Check>; })  // NOLINT(bugprone-macro-parentheses)



int main()
{
#if defined(_DEBUG) && defined(DEBUG_CRT)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF | _CRTDBG_CHECK_EVERY_16_DF | _CRTDBG_CHECK_CRT_DF);
#elif !defined(_DEBUG)
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
#endif

    test("empty", WRAP_FUNC(test_empty));
    test("operations", WRAP_FUNC(test_operations));
    test("invariants", WRAP_FUNC(test_invariants));

    {
        using namespace ankerl::nanobench;

        Bench bench1, bench2, bench3, bench4, bench5, bench6, bench7, bench8, bench9, bench10, bench11, bench12;
        bench1.name("push_front").output(nullptr);
        bench2.name("push_back").output(nullptr);
        bench3.name("pop_front").output(nullptr);
        bench4.name("pop_back").output(nullptr);
        bench5.name("front").output(nullptr);
        bench6.name("back").output(nullptr);
        bench7.name("iterator").output(nullptr);
        bench8.name("reverse_iterator").output(nullptr);
        bench9.name("size").output(nullptr);
        bench10.name("empty").output(nullptr);
        bench11.name("swap").output(nullptr);
        bench12.name("reverse").output(nullptr);

        list<uint64_t> list;

        for (const auto setSize : {10U, 20U, 50U, 100U, 200U, 500U, 1000U, 2000U, 5000U, 10000U})
        {
            auto ensureSize = [&list, setSize]
            {
                while (list.size() < setSize)
                {
                    list.push_back(42);
                }

                while (list.size() > setSize)
                {
                    list.pop_back();
                }
            };

            ensureSize();

            bench1.complexityN(list.size()).run([&list]
            {
                list.push_front(42);
            });

            ensureSize();

            bench2.complexityN(list.size()).run([&list]
            {
                list.push_back(42);
            });

            ensureSize();

            const auto num_epochs = static_cast<std::size_t>(std::floor(std::log10(setSize)));
            const auto num_iterations = static_cast<std::size_t>(std::floor(setSize / num_epochs) - 1);

            boost::ut::expect(num_epochs * num_iterations <= setSize);

            bench3.complexityN(list.size()).epochs(num_epochs).epochIterations(num_iterations).run([&list]
            {
                list.pop_front();
            });

            ensureSize();

            bench4.complexityN(list.size()).epochs(num_epochs).epochIterations(num_iterations).run([&list]
            {
                list.pop_back();
            });

            ensureSize();

            bench5.complexityN(list.size()).run([&list]
            {
                doNotOptimizeAway(list.front());
            });

            ensureSize();

            bench6.complexityN(list.size()).run([&list]
            {
                doNotOptimizeAway(list.back());
            });

            ensureSize();

            bench7.complexityN(list.size()).run([&list]
            {
                const auto end = list.cend();
                for (auto it = list.cbegin(); it != end; ++it)
                    doNotOptimizeAway(*it);
            });

            ensureSize();

            bench8.complexityN(list.size()).run([&list]
            {
                const auto end = list.crend();
                for (auto it = list.crbegin(); it != end; ++it)
                    doNotOptimizeAway(*it);
            });

            ensureSize();

            bench9.complexityN(list.size()).run([&list]
            {
                doNotOptimizeAway(list.size());
            });

            ensureSize();

            bench10.complexityN(list.size()).run([&list]
            {
                doNotOptimizeAway(list.empty());
            });

            ensureSize();

            auto copy = list;

            bench11.complexityN(list.size()).run([&list, &copy]
            {
                using std::swap;
                swap(list, copy);
            });

            doNotOptimizeAway(copy);

            ensureSize();

            bench12.complexityN(list.size()).run([&list]
            {
                list.reverse();
            });
        }

        auto print_complexity = [](Bench const& bench)
        {
            detail::fmt::StreamStateRestorer restorer(std::cout);
            std::cout << std::endl << "| coefficient |   err% | complexity (" << bench.name() << ')' << std::endl <<
                "|------------:|-------:|------------" << std::string(2 + bench.name().size(), '-') << std::endl;
            for (auto const& bigO : bench.complexityBigO() | std::views::take(3))
            {
                std::cout << "|" << std::setw(12) << std::setprecision(1) << std::scientific << bigO.constant() << " ";
                std::cout << "|" << detail::fmt::Number(6, 1, bigO.normalizedRootMeanSquare() * 100.0) << "% ";
                std::cout << "| " << bigO.name();
                std::cout << std::endl;
            }
        };

        // calculate BigO complexity best fit and print the results
        print_complexity(bench1);
        print_complexity(bench2);
        print_complexity(bench3);
        print_complexity(bench4);
        print_complexity(bench5);
        print_complexity(bench6);
        print_complexity(bench7);
        print_complexity(bench8);
        print_complexity(bench9);
        print_complexity(bench10);
        print_complexity(bench11);
        print_complexity(bench12);
    }

    return 0;
}

#undef WRAP_FUNC
