﻿#pragma once

#include <cstdint>
#include <type_traits>
#include <iterator>
#include <algorithm>
#include <limits>
#include <memory>
#include <utility>
#include <initializer_list>
#include <string_view>
#include <optional>

#if _DEBUG
#include <stdexcept>
#include <format>

#define LIST_RELEASE_NOEXCEPT

struct list_exception final : std::runtime_error
{
    template <class... Types>
    [[nodiscard]] explicit list_exception(std::string_view fmt, Types&&... args)
        : runtime_error(std::vformat(fmt, std::make_format_args(args...)))
    {
    }
};

template<class T>
concept shim_formattable =
std::semiregular<std::formatter<std::remove_cvref_t<T>, char>> &&
    requires (std::formatter<std::remove_cvref_t<T>, char> f,
              const std::formatter<std::remove_cvref_t<T>, char> cf,
              T t,
              std::format_context fc,
              std::format_parse_context pc) {
    { f.parse(pc) } -> std::same_as<std::format_parse_context::iterator>;
    { cf.format(t, fc) } -> std::same_as<std::format_context::iterator>;
};
#else
#define LIST_RELEASE_NOEXCEPT noexcept
#endif

template <class T>
class list;

template <class T>
class list_node_base
{
    typedef list_node_base<T> node_type;
    typedef node_type* node_pointer;
    node_pointer next_; // successor node, or first element if head
    node_pointer prev_; // predecessor node, or last element if head

#if _DEBUG
    typedef list<T> list_type;
    const list<T>* head_;
#endif

    template <class T>
    friend class list;
    template <class T, bool Const>
    friend class list_iterator;

public:
    // intentionally non-virtual
    ~list_node_base() = default;

private:
#if _DEBUG
    void check_integrity(bool top = true) const
    {
        if (!next_)
            throw list_exception("Forward list integrity has failed at {}", format_node_pointer(this));

        if (!prev_)
            throw list_exception("Backward list integrity has failed at {}", format_node_pointer(this));

        if (top)
        {
            next_->check_integrity(false);
            prev_->check_integrity(false);

            if (this != head_ && !find_index().has_value())
                throw list_exception("List integrity has failed [entry]: {} not in {}",
                                     format_node_pointer(this), format_node_pointer(head_));
        }

        if (next_->prev_ != this)
            throw list_exception("Forward list integrity has failed [link identity]: {} != this:{}",
                                 format_node_pointer(next_->prev_), format_node_pointer(this));

        if (prev_->next_ != this)
            throw list_exception("Backward list integrity has failed [link identity]: {} != this:{}",
                                 format_node_pointer(prev_->next_), format_node_pointer(this));

        if (next_->head_ != head_)
            throw list_exception("Forward list integrity has failed [different lists]: {}.{} != this:{}.{}",
                                 format_node_pointer(next_), format_node_pointer(next_->head_),
                                 format_node_pointer(this), format_node_pointer(head_));

        if (prev_->head_ != head_)
            throw list_exception("Backward list integrity has failed [different lists]: {}.{} != this:{}.{}",
                                 format_node_pointer(prev_), format_node_pointer(prev_->head_),
                                 format_node_pointer(this), format_node_pointer(head_));
    }

    static std::string format_node_pointer(const node_type* ptr) noexcept
    {
        if (!ptr)
            return "null";

        auto formatted = ptr->format();
        return formatted.has_value()
                   ? std::format("{}[{}]", format_pointer(ptr), formatted.value())
                   : std::format("{}", format_pointer(ptr));
    }

    static std::string format_pointer(const auto* ptr)
    {
        if (!ptr)
            throw list_exception("nullptr cannot be formatted using this routine");

        const auto ptr_num = reinterpret_cast<std::uintptr_t>(static_cast<const void*>(ptr));
        return std::format("0x{:0{}x}", ptr_num, sizeof std::size_t * 2);
    }

    virtual std::optional<std::string> format() const = 0;

protected:
    std::optional<std::size_t> find_index() const
    {
        if (this == head_)
            return std::nullopt;

        std::size_t result = 0;
        auto first = head_->next_;

        while (first != head_)
        {
            if (first == this)
                return result;

            first = first->next_;
            ++result;
        }

        return std::nullopt;
    }
#endif
};

template <class T, bool Const>
class list_iterator final
{
    typedef list_node_base<T> node_base_type;
    typedef typename list<T>::list_node node_type;
    typedef list_iterator<T, Const> iterator;
    typedef std::conditional_t<Const, std::add_const_t<node_base_type>, node_base_type>* node_base_pointer;
    typedef std::conditional_t<Const, std::add_const_t<node_type>, node_type>* node_pointer;
    typedef std::conditional_t<Const, std::add_const_t<T>, T> specific_value_type;

    node_base_pointer node_;

    [[nodiscard]] node_base_pointer node() const
    {
#if _DEBUG
        ensure_non_default();
#endif

        return node_;
    }

    [[nodiscard]] explicit list_iterator(node_base_pointer node) : node_(node)
    {
#if _DEBUG
        if (node_)
            node_->check_integrity();
#endif
    }

    template <class T>
    friend class list;
    template <class T>
    friend class list_node_base;

#if _DEBUG
    void ensure_non_default() const
    {
        if (!node_)
            throw list_exception("Cannot operate on default-initialized {}: {}", typeid(iterator).name(),
                                 node_base_type::format_pointer(this));
    }
#endif

public:
    typedef std::ptrdiff_t difference_type;
    typedef std::bidirectional_iterator_tag iterator_category;
    typedef T value_type;
    typedef std::add_pointer_t<specific_value_type> pointer;
    typedef std::add_lvalue_reference_t<specific_value_type> reference;

    [[nodiscard]] list_iterator() : list_iterator(nullptr)
    {
    }

    list_iterator(const list_iterator& other) : node_{other.node_}
    {
    }

    list_iterator(list_iterator&& other) noexcept : node_{other.node_}
    {
    }

    list_iterator& operator=(list_iterator other)
    {
        using std::swap;
        swap(*this, other);
        return *this;
    }

    friend void swap(list_iterator& lhs, list_iterator& rhs) noexcept
    {
        using std::swap;
        swap(lhs.node_, rhs.node_);
    }

    ~list_iterator()
    {
    }

    reference operator*() const LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        ensure_non_default();

        if (node_ == node_->head_)
            throw list_exception("Cannot dereference list head: {}", node_base_type::format_node_pointer(node_));

        node_->check_integrity();
#endif

        return **static_cast<node_pointer>(node_);
    }

    pointer operator->() const LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        ensure_non_default();

        if (node_ == node_->head_)
            throw list_exception("Cannot address list head: {}", node_base_type::format_node_pointer(node_));

        node_->check_integrity();
#endif

        return static_cast<node_pointer>(node_)->address_of_value();
    }

    iterator& operator++() LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        ensure_non_default();
#endif

        node_ = static_cast<node_base_pointer>(node_->next_);
#if _DEBUG
        node_->check_integrity();
#endif
        return *this;
    }

    iterator operator++(int) LIST_RELEASE_NOEXCEPT
    {
        iterator tmp = *this;
        ++*this;
        return tmp;
    }

    iterator& operator--() LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        ensure_non_default();
#endif

        node_ = static_cast<node_base_pointer>(node_->prev_);
#if _DEBUG
        node_->check_integrity();
#endif
        return *this;
    }

    iterator operator--(int) LIST_RELEASE_NOEXCEPT
    {
        iterator tmp = *this;
        --*this;
        return tmp;
    }

    [[nodiscard]] friend bool operator==(const iterator& x, const iterator& y) noexcept
    {
        return x.node_ == y.node_;
    }
};

template <class T>
class list final : list_node_base<T>
{
    typedef list_node_base<T> node_base_type;

public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef list_iterator<T, false> iterator;
    typedef list_iterator<T, true> const_iterator;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    explicit list() noexcept
    {
        initialize_impl();
    }

    template <class Iter>
        requires std::input_iterator<Iter>
    list(Iter first, Iter last) : list()
    {
        append(first, last);
    }

    list(const list& x) : list(x.begin(), x.end())
    {
    }

    list(list&& x) LIST_RELEASE_NOEXCEPT : list()
    {
        swap(x);
    }

    list(std::initializer_list<value_type> x) : list(x.begin(), x.end())
    {
    }

    ~list()
    {
        try
        {
            free_non_head(this);
        }
        catch (...)
        {
        }
    }

    list& operator=(const list& x)
    {
        if (this != std::addressof(x))
            assign(x.begin(), x.end());

        return *this;
    }

    list& operator=(list&& x) LIST_RELEASE_NOEXCEPT
    {
        if (this == std::addressof(x))
            return *this;

        clear();
        swap(x);

        return *this;
    }

    list& operator=(std::initializer_list<value_type> x)
    {
        assign(x.begin(), x.end());
        return *this;
    }

    template <class Iter>
        requires std::input_iterator<Iter>
    void assign(Iter first, Iter last)
    {
        iterator it = begin();
        iterator end = list::end();
        for (; first != last && it != end; ++first, ++it)
            *it = *first;
        if (it == end)
            append(first, last);
        else
            erase_until_end(it);
    }

    void assign(std::initializer_list<value_type> x)
    {
        assign(x.begin(), x.end());
    }

    [[nodiscard]] iterator begin() noexcept
    {
        return iterator(node_base_type::next_);
    }

    [[nodiscard]] const_iterator begin() const noexcept
    {
        return cbegin();
    }

    [[nodiscard]] iterator end() noexcept
    {
        return iterator(this);
    }

    [[nodiscard]] const_iterator end() const noexcept
    {
        return cend();
    }

    [[nodiscard]] reverse_iterator rbegin() noexcept
    {
        return reverse_iterator(end());
    }

    [[nodiscard]] const_reverse_iterator rbegin() const noexcept
    {
        return crbegin();
    }

    [[nodiscard]] reverse_iterator rend() noexcept
    {
        return reverse_iterator(begin());
    }

    [[nodiscard]] const_reverse_iterator rend() const noexcept
    {
        return crend();
    }

    [[nodiscard]] const_iterator cbegin() const noexcept
    {
        return const_iterator(node_base_type::next_);
    }

    [[nodiscard]] const_iterator cend() const noexcept
    {
        return const_iterator(this);
    }

    [[nodiscard]] const_reverse_iterator crbegin() const noexcept
    {
        return const_reverse_iterator(end());
    }

    [[nodiscard]] const_reverse_iterator crend() const noexcept
    {
        return const_reverse_iterator(begin());
    }

    [[nodiscard]] reference front()
    {
#if _DEBUG
        if (empty())
            throw list_exception("The list is empty: {}", node_base_type::format_node_pointer(this));
#endif

        return *begin();
    }

    [[nodiscard]] const_reference front() const
    {
#if _DEBUG
        if (empty())
            throw list_exception("The list is empty: {}", node_base_type::format_node_pointer(this));
#endif

        return *cbegin();
    }

    [[nodiscard]] reference back()
    {
#if _DEBUG
        if (empty())
            throw list_exception("The list is empty: {}", node_base_type::format_node_pointer(this));
#endif

        return *--end();
    }

    [[nodiscard]] const_reference back() const
    {
#if _DEBUG
        if (empty())
            throw list_exception("The list is empty: {}", node_base_type::format_node_pointer(this));
#endif

        return *--cend();
    }

    [[nodiscard]] bool empty() const LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        if ((size_impl() == 0) != empty_impl())
            throw list_exception("List size integrity has failed: size:{} empty:{} for {}", size_impl(), empty_impl(),
                                 node_base_type::format_node_pointer(this));
#endif

        return empty_impl();
    }

    [[nodiscard]] size_type size() const LIST_RELEASE_NOEXCEPT
    {
#if _DEBUG
        if ((size_impl() == 0) != empty_impl())
            throw list_exception("List size integrity has failed: size:{} empty:{} for {}", size_impl(), empty_impl(),
                                 node_base_type::format_node_pointer(this));
#endif

        return size_impl();
    }

    [[nodiscard]] constexpr size_type max_size() const noexcept
    {
        return std::min(static_cast<size_type>(std::numeric_limits<difference_type>::max()),
                        std::numeric_limits<size_type>::max() / sizeof(list_node));
    }

    template <class... Args>
    reference emplace_front(Args&&... args)
    {
#if _DEBUG
        if (size() >= max_size())
            throw list_exception("List is too large ({}): cannot emplace an element to {} above the size of {}", size(),
                                 node_base_type::format_node_pointer(this), max_size());
#endif

        ++size_;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        auto* next = node_base_type::next_;

#if _DEBUG
        if (next->prev_ != this)
            throw list_exception("List integrity has failed [first]: {} for {}",
                                 node_base_type::format_node_pointer(next), node_base_type::format_node_pointer(this));
#endif

        auto* node = new list_node(std::forward<Args>(args)...);
#if _DEBUG
        node->head_ = this;
#endif
        node->next_ = next;
        node->prev_ = this;
        next->prev_ = node;
        node_base_type::next_ = node;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        return **node;
    }

    void pop_front()
    {
#if _DEBUG
        if (empty())
            throw list_exception("An element cannot be popped from an empty list: {}",
                                 node_base_type::format_node_pointer(this));
#endif

        --size_;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        auto* entry = node_base_type::next_;
        auto* next = entry->next_;

#if _DEBUG
        if (entry->prev_ != this)
            throw list_exception("List integrity has failed [head]: {} for {}",
                                 node_base_type::format_node_pointer(entry), node_base_type::format_node_pointer(this));

        if (next->prev_ != entry)
            throw list_exception("List integrity has failed [first]: {} and {} for {}",
                                 node_base_type::format_node_pointer(entry), node_base_type::format_node_pointer(next),
                                 node_base_type::format_node_pointer(this));
#endif

        node_base_type::next_ = next;
        next->prev_ = this;

        free_node(entry);
    }

    template <class... Args>
    reference emplace_back(Args&&... args)
    {
#if _DEBUG
        if (size() >= max_size())
            throw list_exception("List is too large ({}): cannot emplace an element to {} above the size of {}", size(),
                                 node_base_type::format_node_pointer(this), max_size());
#endif

        ++size_;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        auto* prev = node_base_type::prev_;

#if _DEBUG
        if (prev->next_ != this)
            throw list_exception("List integrity has failed [last]: {} for {}",
                                 node_base_type::format_node_pointer(prev), node_base_type::format_node_pointer(this));
#endif

        auto* node = new list_node(std::forward<Args>(args)...);
#if _DEBUG
        node->head_ = this;
#endif
        node->next_ = this;
        node->prev_ = prev;
        prev->next_ = node;
        node_base_type::prev_ = node;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        return **node;
    }

    void pop_back()
    {
#if _DEBUG
        if (empty())
            throw list_exception("An element cannot be popped from an empty list: {}",
                                 node_base_type::format_node_pointer(this));
#endif

        --size_;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        auto* entry = node_base_type::prev_;
        auto* prev = entry->prev_;

#if _DEBUG
        if (entry->next_ != this)
            throw list_exception("List integrity has failed [head]: {} for {}",
                                 node_base_type::format_node_pointer(entry), node_base_type::format_node_pointer(this));

        if (prev->next_ != entry)
            throw list_exception("List integrity has failed [last]: {} and {} for {}",
                                 node_base_type::format_node_pointer(prev), node_base_type::format_node_pointer(entry),
                                 node_base_type::format_node_pointer(this));
#endif

        node_base_type::prev_ = prev;
        prev->next_ = this;

        free_node(entry);
    }

    void push_front(const value_type& x)
    {
        emplace_front(x);
    }

    void push_front(value_type&& x)
    {
        emplace_front(std::move(x));
    }

    void push_back(const value_type& x)
    {
        emplace_back(x);
    }

    void push_back(value_type&& x)
    {
        emplace_back(std::move(x));
    }

    void swap(list& x) LIST_RELEASE_NOEXCEPT
    {
        if (this == std::addressof(x))
            return;

#if _DEBUG
        node_base_type::check_integrity();
#endif

        using std::swap;
        swap(node_base_type::next_, x.next_);
        swap(node_base_type::prev_, x.prev_);
        swap(size_, x.size_);

        if (!size_impl())
            initialize_impl_core();
        else
            node_base_type::prev_->next_ = node_base_type::next_->prev_ = this;

        if (!x.size_impl())
            x.initialize_impl_core();
        else
            x.prev_->next_ = x.next_->prev_ = std::addressof(x);

#if _DEBUG
        reset_head();
        x.reset_head();

        node_base_type::check_integrity();
#endif
    }

    void clear()
    {
        free_non_head(this);
        initialize_impl();
    }

    void reverse() LIST_RELEASE_NOEXCEPT
    {
        node_base_type* const head = this;
        auto* current = head;

        do
        {
            using std::swap;

            swap(current->next_, current->prev_);

            current = current->prev_;
        }
        while (current != head);
    }

private:
    size_type size_;

#if _DEBUG
    std::optional<std::string> format() const override
    {
        return std::format("HEAD size={}", size_impl());
    }
#endif

    [[nodiscard]] bool empty_impl() const noexcept
    {
        return node_base_type::next_ == this;
    }

    [[nodiscard]] size_type size_impl() const noexcept
    {
        return size_;
    }

    void initialize_impl() noexcept
    {
        size_ = 0;
        initialize_impl_core();
#if _DEBUG
        node_base_type::head_ = this;
#endif
    }

    void initialize_impl_core() noexcept
    {
        node_base_type::next_ = node_base_type::prev_ = this;
    }

    static void free_non_head(node_base_type* const head)
    {
        head->prev_->next_ = nullptr;

        auto* current = head->next_;
        for (node_base_type* next; current; current = next)
        {
            next = current->next_;
            free_node(current);
        }
    }

    static void free_node(node_base_type* node)
    {
#if _DEBUG
        if (node->head_ == node)
            throw list_exception("List integrity has failed: cannot free head {}",
                                 node_base_type::format_node_pointer(node));
#endif

        delete static_cast<list_node*>(node);
    }

    template <class Iter>
        requires std::input_iterator<Iter>
    void append(Iter& first, const Iter& last)
    {
#if _DEBUG
        node_base_type::check_integrity();
#endif

        if (first == last)
            return;

        auto* prev = node_base_type::prev_;
        auto size = size_;

        for (; first != last; ++first)
        {
            ++size;

            auto* node = new list_node(*first);
#if _DEBUG
            node->head_ = this;
#endif
            node->prev_ = prev;
            prev->next_ = node;
            prev = node;
        }

        prev->next_ = this;
        node_base_type::prev_ = prev;

#if _DEBUG
        if (size >= max_size())
            throw list_exception("List is too large ({}): cannot append elements to {} above the size of {}", size,
                                 node_base_type::format_node_pointer(this), max_size());

        node_base_type::check_integrity();
#endif

        size_ = size;
    }

    template <bool Const>
    void erase_until_end(list_iterator<T, Const>& pos)
    {
#if _DEBUG
        node_base_type::check_integrity();
#endif

        {
            auto* entry = pos.node();
            auto* prev = entry->prev_;

#if _DEBUG
            entry->check_integrity();
#endif

            entry->prev_ = nullptr;

            node_base_type::prev_ = prev;
            prev->next_ = this;
        }

        iterator end = list::end();
        while (pos != end)
        {
#if _DEBUG
            if (!size_impl())
                throw list_exception("List size integrity has failed [erase]: {}",
                                     node_base_type::format_node_pointer(this));
#endif

            --size_;

            auto* entry = pos.node();
            ++pos;

#if _DEBUG
            if (pos != end && pos.node()->prev_ != entry)
                throw list_exception("List integrity has failed [erase]: {} and {} for {}",
                                     node_base_type::format_node_pointer(entry),
                                     node_base_type::format_node_pointer(pos.node()),
                                     node_base_type::format_node_pointer(this));
#endif

            free_node(entry);
        }

#if _DEBUG
        node_base_type::check_integrity();
#endif
    }

#if _DEBUG
    void reset_head() const
    {
        for (auto* current = node_base_type::next_; current != this; current = current->next_)
            current->head_ = this;
    }
#endif

    template <class T>
    friend class list_node_base;
    template <class T, bool Const>
    friend class list_iterator;

    class list_node final : list_node_base<T>
    {
        typedef list_node_base<T> node_base_type;

        template <class T>
        friend class list;
        template <class T, bool Const>
        friend class list_iterator;

        T value_;

        template <typename... Args>
        [[nodiscard]] explicit list_node(Args&&... args) noexcept(std::is_nothrow_constructible_v<T, Args>) : value_(std::forward<Args>(args)...)
        {
        }

        ~list_node() noexcept
        {
        }

        list_node(const list_node& other) = delete;
        list_node(list_node&& other) noexcept = delete;
        list_node& operator=(const list_node& other) = delete;
        list_node& operator=(list_node&& other) noexcept = delete;

        std::add_lvalue_reference_t<std::add_const_t<T>> operator*() const noexcept
        {
            return *address_of_value();
        }

        std::add_lvalue_reference_t<T> operator*() noexcept
        {
            return *address_of_value();
        }

        [[nodiscard]] std::add_pointer_t<std::add_const_t<T>> address_of_value() const noexcept
        {
            return std::addressof(value_);
        }

        [[nodiscard]] std::add_pointer_t<T> address_of_value() noexcept
        {
            return std::addressof(value_);
        }

#if _DEBUG
        std::optional<std::string> format() const override
        {
            auto index = node_base_type::find_index();
            if constexpr (shim_formattable<T>)
            {
                return index.has_value()
                    ? std::format("{{{}}} {}", index.value(), **this)
                    : std::format("{{?}} {}", **this);
            }
            else
            {
                return index.has_value()
                    ? std::format("{{{}}} @{}", index.value(), node_base_type::format_pointer(address_of_value()))
                    : std::format("{{?}} @{}", node_base_type::format_pointer(address_of_value()));
            }
        }
#endif
    };
};

template <class T>
void swap(list<T>& x, list<T>& y)
noexcept(noexcept(x.swap(y)))
{
    x.swap(y);
}

template <class T>
[[nodiscard]] bool operator==(const list<T>& x, const list<T>& y)
{
    return x.size() == y.size()
        && std::equal(x.cbegin(), x.cend(), y.cbegin());
}

template <class T>
[[nodiscard]] auto operator<=>(const list<T>& x, const list<T>& y)
{
    return std::lexicographical_compare_three_way(x.cbegin(), x.cend(),
                                                  y.cbegin(), y.cend());
}
