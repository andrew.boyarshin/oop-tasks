﻿#pragma once

#include <cstdint>
#include <string>
#include <string_view>
#include <optional>
#include <format>
#include <memory>
#include <cmath>
#include <ranges>

#include "../T1/T1.h"

struct object
{
    [[nodiscard]] object() noexcept
    {
        ++count_;
    }

    virtual ~object()
    {
        --count_;
    }

    object(const object&) noexcept : object()
    {
    }

    object(object&&) noexcept : object()
    {
    }

    object& operator=(const object&)
    {
        return *this;
    }

    object& operator=(object&&) noexcept
    {
        return *this;
    }

    [[nodiscard]] virtual std::string format() const noexcept = 0;

    [[nodiscard]] static auto count() noexcept
    {
        return count_;
    }

private:
    inline static std::size_t count_ = 0u;
};

class task_tag
{
};

template <class Result = void>
struct task : virtual object, virtual task_tag
{
    using result_type = Result;

    virtual result_type execute() = 0;
};

struct named : virtual object
{
    [[nodiscard]] explicit named() = default;

    named(const named& other)
        : object{ other },
        name_{ other.name_ }
    {
    }

    named(named&& other) noexcept
        : object{ std::move(other) },
        name_{ std::move(other.name_) }
    {
    }

    named& operator=(const named& other)
    {
        if (this == std::addressof(other))
            return *this;
        name_ = other.name_;
        return *this;
    }

    named& operator=(named&& other) noexcept
    {
        if (this == std::addressof(other))
            return *this;
        name_ = std::move(other.name_);
        return *this;
    }

    [[nodiscard]] const std::string& name() const noexcept
    {
        return name_;
    }

    template<class T>
    void name(T&& name)
    {
        name_ = std::forward<T>(name);
    }

    [[nodiscard]] std::string format() const noexcept override
    {
        return name_;
    }

private:
    std::string name_;
};

enum class arithmetic_op
{
    addition,
    subtraction,
    multiplication,
    division
};

template <class Result>
struct lazy_task : task<Result>
{
    using task<Result>::result_type;

    result_type execute() override
    {
        if (!result_.has_value())
            result_ = execute_core();

        return result_.value();
    }

    [[nodiscard]] auto result() const noexcept
    {
        return result_;
    }

    [[nodiscard]] std::string format() const noexcept override
    {
        return result_.has_value()
            ? std::format("{} = {}", format_core(), result_.value())
            : std::format("{} = ?", format_core());
    }

protected:
    [[nodiscard]] virtual result_type execute_core() const = 0;
    [[nodiscard]] virtual std::string format_core() const noexcept = 0;

private:
    std::optional<result_type> result_;
};

struct arithmetic_task : lazy_task<double>, named
{
    [[nodiscard]] auto arg1() const noexcept
    {
        return arg1_;
    }

    [[nodiscard]] auto arg2() const noexcept
    {
        return arg2_;
    }

    [[nodiscard]] std::string format() const noexcept override
    {
        auto lazy = lazy_task<double>::format();
        auto named = named::format();
        return named.empty()
            ? lazy
            : std::format("[{}] {}", named, lazy);
    }

protected:
    [[nodiscard]] explicit arithmetic_task(result_type arg1, result_type arg2) : named(), arg1_(arg1), arg2_(arg2)
    {
    }

    [[nodiscard]] std::string format_core() const noexcept override
    {
        return std::format("{} {} {}", arg1_, operation_symbol_core(), arg2_);
    }

    [[nodiscard]] virtual std::string_view operation_symbol_core() const noexcept = 0;

    result_type arg1_, arg2_;

private:
    std::optional<result_type> result_;
};

struct arithmetic_addition_task final : arithmetic_task
{
    [[nodiscard]] explicit arithmetic_addition_task(result_type arg1, result_type arg2) : arithmetic_task(arg1, arg2)
    {
    }

protected:
    [[nodiscard]] result_type execute_core() const override
    {
        return arg1_ + arg2_;
    }

    [[nodiscard]] std::string_view operation_symbol_core() const noexcept override
    {
        const static std::string symbol = "+";
        return symbol;
    }
};

struct arithmetic_subtraction_task final : arithmetic_task
{
    [[nodiscard]] explicit arithmetic_subtraction_task(result_type arg1, result_type arg2) : arithmetic_task(arg1, arg2)
    {
    }

protected:
    [[nodiscard]] result_type execute_core() const override
    {
        return arg1_ - arg2_;
    }

    [[nodiscard]] std::string_view operation_symbol_core() const noexcept override
    {
        const static std::string symbol = "-";
        return symbol;
    }
};

struct arithmetic_multiplication_task final : arithmetic_task
{
    [[nodiscard]] explicit arithmetic_multiplication_task(result_type arg1, result_type arg2) : arithmetic_task(arg1, arg2)
    {
    }

protected:
    [[nodiscard]] result_type execute_core() const override
    {
        return arg1_ * arg2_;
    }

    [[nodiscard]] std::string_view operation_symbol_core() const noexcept override
    {
        const static std::string symbol = "*";
        return symbol;
    }
};

struct arithmetic_division_task final : arithmetic_task
{
    [[nodiscard]] explicit arithmetic_division_task(result_type arg1, result_type arg2) : arithmetic_task(arg1, arg2)
    {
    }

protected:
    [[nodiscard]] result_type execute_core() const override
    {
        if (std::fpclassify(arg2_) == FP_ZERO)
            throw std::runtime_error(std::format("Division of {} by zero", arg1_));

        return arg1_ / arg2_;
    }

    [[nodiscard]] std::string_view operation_symbol_core() const noexcept override
    {
        const static std::string symbol = "/";
        return symbol;
    }
};

struct queue_task final : task<>
{
    [[nodiscard]] explicit queue_task(std::shared_ptr<list<std::shared_ptr<object>>> container, std::shared_ptr<object> object) : container_(container), object_(object)
    {
    }

    void execute() override
    {
        container_->push_back(object_);
    }

    [[nodiscard]] std::string format() const noexcept override
    {
        return std::format("push {{{}}} to {{{}}}", object_ ? object_->format() : "<null>", container_ ? std::format("size={}", container_->size()) : "<null>");
    }

private:
    std::shared_ptr<list<std::shared_ptr<object>>> container_;
    std::shared_ptr<object> object_;
};

struct object_count_task final : lazy_task<std::size_t>
{
    [[nodiscard]] explicit object_count_task(std::shared_ptr<list<std::shared_ptr<object>>> container) : container_(container)
    {
    }

private:
    [[nodiscard]] result_type execute_core() const override
    {
        return container_->size();
    }

    [[nodiscard]] std::string format_core() const noexcept override
    {
        return std::format("object count in {{{}}}", container_ ? std::format("size={}", container_->size()) : "<null>");
    }

    std::shared_ptr<list<std::shared_ptr<object>>> container_;
};

struct task_func_count_task final : lazy_task<std::size_t>
{
    [[nodiscard]] explicit task_func_count_task(std::shared_ptr<list<std::shared_ptr<object>>> container) : container_(container)
    {
    }

private:
    [[nodiscard]] result_type execute_core() const override
    {
        return std::ranges::count_if(*container_, [](auto&& v) { return std::dynamic_pointer_cast<task_tag>(v) && !std::dynamic_pointer_cast<task<>>(v); });
    }

    [[nodiscard]] std::string format_core() const noexcept override
    {
        return std::format("task functors count in {{{}}}", container_ ? std::format("size={}", container_->size()) : "<null>");
    }

    std::shared_ptr<list<std::shared_ptr<object>>> container_;
};

struct clear_task final : task<>
{
    [[nodiscard]] explicit clear_task(std::shared_ptr<list<std::shared_ptr<object>>> container) : container_(container)
    {
    }

    void execute() override
    {
        container_->clear();
    }

    [[nodiscard]] std::string format() const noexcept override
    {
        return std::format("clear {{{}}}", container_ ? std::format("size={}", container_->size()) : "<null>");
    }

private:

    std::shared_ptr<list<std::shared_ptr<object>>> container_;
};

struct total_object_count_task final : lazy_task<std::size_t>
{
    [[nodiscard]] explicit total_object_count_task()
    {
    }

private:
    [[nodiscard]] result_type execute_core() const override
    {
        return object::count();
    }

    [[nodiscard]] std::string format_core() const noexcept override
    {
        return { "total object count" };
    }
};


template <class T>
concept proper_object = std::is_base_of_v<object, T>
&& std::is_destructible_v<T> && !std::is_trivially_destructible_v<T>
&& std::is_copy_constructible_v<T> && std::is_move_constructible_v<T>
&& std::is_copy_assignable_v<T> && std::is_move_assignable_v<T>;

static_assert(proper_object<arithmetic_addition_task>);
static_assert(proper_object<arithmetic_subtraction_task>);
static_assert(proper_object<arithmetic_multiplication_task>);
static_assert(proper_object<arithmetic_division_task>);
static_assert(proper_object<queue_task>);
static_assert(proper_object<object_count_task>);
static_assert(proper_object<task_func_count_task>);
static_assert(proper_object<clear_task>);
static_assert(proper_object<total_object_count_task>);

